// Copyright 2018 Ciro DE CARO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#[macro_use]
extern crate log;
extern crate env_logger;
#[macro_use]
extern crate markup;
#[macro_use]
extern crate structopt;
#[macro_use]
extern crate clap;
extern crate clap_verbosity_flag;
extern crate config;

mod settings;
mod templates;

use clap::App;

const DEFAULT_SETTINGS_NAME: &str = "Settings";

fn main() {
    // start env_logger logger
    env_logger::init();
    // The YAML file is found relative to the current file, similar to how modules are found
    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml).get_matches();
    // load the configuration file
    // have to use match to display a message, otherwise it just works
    let config_name = matches.value_of("config").unwrap_or_else(|| {
        info!(
            "loading configuration with default settings name: {}",
            DEFAULT_SETTINGS_NAME
        );
        DEFAULT_SETTINGS_NAME
    });
    let config = settings::get(config_name).unwrap();

    // can be configured with the prefix APP and the name of the key
    if config["debug"] != "false" {
        println!("{:?}", config);
    }
    let no_export = value_t!(matches.value_of("no-export"), bool).unwrap_or_default();
    let t = value_t!(matches.value_of("type"), String).unwrap_or_default();
    let name = matches.value_of("name").unwrap();
    
    let new_component = match Some(&*t.to_string()) {
        Some("WebComponent") => templates::web_component::new(String::from(name), no_export),
        _ => panic!(format!("no component of type: {}", t)),
    };

    println!("created component: {}", new_component);
}
