markup::define! {
    WebComponent(name: String, export: bool) {
        script {
            "\n"
            @if *export == true {
                "export default class "
            } else {
                "class "
            }
            { name.to_owned() }
            "{ "
            "\n\n"
            "};"
            "\n"
        }
        "\n"
        template {
            "\n"
            ""
            "\n"
        }
        "\n"
        style {
            "\n"
            ""
            "\n"
        }
    }
}

pub fn new(name: String, no_export: bool) -> WebComponent {
    WebComponent { name, export: !no_export }
}
